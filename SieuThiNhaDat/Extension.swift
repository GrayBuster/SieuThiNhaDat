//
//  Extension.swift
//  SieuThiNhaDat
//
//  Created by gray buster on 8/3/18.
//  Copyright © 2018 gray buster. All rights reserved.
//

import Foundation
import UIKit


extension UISegmentedControl {
    func removeBorder(){
        //Set background color of segmented
        let backgroundImage = UIImage.getColoredRectImageWith(color: UIColor(red: 247/255, green: 247/255, blue: 247/255, alpha: 1.0).cgColor, andSize: self.bounds.size)
        self.setBackgroundImage(backgroundImage, for: .normal, barMetrics: .default)
        self.setBackgroundImage(backgroundImage, for: .selected, barMetrics: .default)
        self.setBackgroundImage(backgroundImage, for: .highlighted, barMetrics: .default)
    
       //Set text black color to default
        self.setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.black,NSAttributedStringKey.font: UIFont(name: "Arial", size:12)!], for: .normal)
       //Set text green color to selected
        self.setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor(red: 114/255, green: 188/255, blue: 93/255, alpha: 1.0),NSAttributedStringKey.font: UIFont(name: "Arial", size:12)!], for: .selected)
    }
    
    //Add underline to Segment
    
    func addUnderlineForSelectedSegment(){
        removeBorder()
        let underlineWidth: CGFloat = self.bounds.size.width / CGFloat(self.numberOfSegments)
        let underlineHeight: CGFloat = 2.0
        let underlineXPosition = CGFloat(selectedSegmentIndex * Int(underlineWidth))
        let underLineYPosition = self.bounds.size.height - 1.0
        let underlineFrame = CGRect(x: underlineXPosition, y: underLineYPosition, width: underlineWidth, height: underlineHeight)
        let underline = UIView(frame: underlineFrame)
        underline.backgroundColor = UIColor(red: 67/255, green: 129/255, blue: 244/255, alpha: 1.0)
        underline.tag = 1
        self.addSubview(underline)
    }
    
    //Change underline position
    
    func changeUnderlinePosition(){
        guard let underline = self.viewWithTag(1) else {return}
        let underlineFinalXPosition = (self.bounds.width / CGFloat(self.numberOfSegments)) * CGFloat(selectedSegmentIndex)
        UIView.animate(withDuration: 0.1, animations: {
            underline.frame.origin.x = underlineFinalXPosition
        })
    }
    
    //Change boder color of Segment
    
    func changeBorderColor(_ color: UIColor) {
        for selectView in subviews{
            selectView.layer.borderColor = color.cgColor
            selectView.layer.borderWidth = CGFloat(1.2)
            selectView.layer.cornerRadius = CGFloat(6)
            //selectView.layer.masksToBounds = true
        }
    }
}

extension UITabBar {
    
    //Set font ,size and color of tab bar item title
    func change(fontSize size: CGFloat, font: String,underlineColor: UIColor?,fontColor: UIColor?,fontSelectedColor: UIColor?) {
        if underlineColor != nil && fontColor != nil && fontSelectedColor != nil {
            self.selectionIndicatorImage = UIImage().createSelectionIndicator(color: underlineColor!, size: CGSize(width: self.frame.width/CGFloat(self.items!.count), height: self.frame.height), lineWidth: 2.0)
            for item in self.items! {
                item.setTitleTextAttributes([NSAttributedStringKey.font: UIFont(name: font, size:size)!, NSAttributedStringKey.foregroundColor: fontColor!], for: .normal)
                item.setTitleTextAttributes([NSAttributedStringKey.font: UIFont(name: font, size:size)!, NSAttributedStringKey.foregroundColor: fontSelectedColor!], for: .selected)
            }
        } else if fontColor != nil && fontSelectedColor != nil {
            for item in self.items! {
                item.setTitleTextAttributes([NSAttributedStringKey.font: UIFont(name: font, size:size)!, NSAttributedStringKey.foregroundColor: fontColor!], for: .normal)
                item.setTitleTextAttributes([NSAttributedStringKey.font: UIFont(name: font, size:size)!, NSAttributedStringKey.foregroundColor: fontSelectedColor!], for: .selected)
            }
        }
    }
    
    //Add Tab bar seperator
    func setupTabBarSeparators() {
        let itemWidth = floor(self.frame.size.width / CGFloat(self.items!.count))
        
        // this is the separator width.  0.5px matches the line at the top of the tab bar
        let separatorWidth: CGFloat = 0.5
        
        // iterate through the items in the Tab Bar, except the last one
        for i in 0...(self.items!.count - 1) {
            // make a new separator at the end of each tab bar item
            let separator = UIView(frame: CGRect(x: itemWidth * CGFloat(i + 1) - CGFloat(separatorWidth / 2), y: 0, width: CGFloat(separatorWidth), height: self.frame.size.height))
            
            // set the color to seperator (default line color for tab bar)
            separator.backgroundColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)
            
            self.addSubview(separator)
        }
    }
    
   
}

extension UIView {
    func addBottomBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x:0, y:self.frame.size.height - width, width:self.frame.size.width, height:width)
        self.layer.addSublayer(border)
    }
}


extension UIImage {
    
    // Highlight selected tab
    func createSelectionIndicator(color: UIColor, size: CGSize, lineWidth: CGFloat) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(CGRect(x: 0,y: size.height - lineWidth, width: size.width,height: lineWidth))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
    class func imageWithColor(color: UIColor, size: CGSize) -> UIImage {
        let rect: CGRect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
    
    class func getColoredRectImageWith(color: CGColor, andSize size: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        let graphicsContext = UIGraphicsGetCurrentContext()
        graphicsContext?.setFillColor(color)
        let rectangle = CGRect(x: 0.0, y: 0.0, width: size.width, height: size.height)
        graphicsContext?.fill(rectangle)
        let rectangleImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return rectangleImage!
    }
}

// Extension data

extension Data {
    
    mutating func append(_ string: String, using encoding: String.Encoding = .utf8) {
        if let data = string.data(using: encoding) {
            append(data)
        }
    }
}

extension UIViewController {
    func showAlert(msg: String) {
        let alert = UIAlertController(title: "Cảnh báo!", message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
