//
//  APIModel.swift
//  SieuThiNhaDat
//
//  Created by gray buster on 8/7/18.
//  Copyright © 2018 gray buster. All rights reserved.
//

import Foundation
import SwiftyJSON

class User {
    
//    func encode(with aCoder: NSCoder) {
//        aCoder.encode(username, forKey: "username")
//        aCoder.encode(currencies, forKey: "currencies")
//        aCoder.encode(uid, forKey: "uid")
//        aCoder.encode(fcm_project_id, forKey: "fcm_project_id")
//        aCoder.encode(db, forKey: "db")
//        aCoder.encode(inbox_action, forKey: "inbox_action")
//        aCoder.encode(is_admin, forKey: "is_admin")
//        aCoder.encode(partner_id, forKey: "partner_id")
//        aCoder.encode(server_version, forKey: "server_version")
//        aCoder.encode(user_context, forKey: "user_context")
//        aCoder.encode(name, forKey: "name")
//        aCoder.encode(server_version_info, forKey: "server_version_info")
//        aCoder.encode(company_id, forKey: "company_id")
//        aCoder.encode(session_id, forKey: "session_id")
//        aCoder.encode(device_subscription_ids, forKey: "device_subscription_ids")
//        aCoder.encode(user_companies, forKey: "user_companies")
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        self.username = aDecoder.decodeObject(forKey: "username") as! String
//        self.currencies = aDecoder.decodeObject(forKey: "currencies") as! Currencies
//        self.uid = aDecoder.decodeObject(forKey: "uid") as! Int
//        self.fcm_project_id = aDecoder.decodeObject(forKey: "fcm_project_id") as! String
//        self.db = aDecoder.decodeObject(forKey: "db") as! String
//        self.inbox_action = aDecoder.decodeObject(forKey: "inbox_action") as! Int
//        self.is_admin = aDecoder.decodeObject(forKey: "is_admin") as! Bool
//        self.partner_id = aDecoder.decodeObject(forKey: "partner_id") as! Int
//        self.server_version = aDecoder.decodeObject(forKey: "server_version") as! String
//        self.user_context = aDecoder.decodeObject(forKey: "user_context") as! UserContext
//        self.name = aDecoder.decodeObject(forKey: "name") as! String
//        self.server_version_info = aDecoder.decodeObject(forKey: "server_version_info") as! [Any]
//        self.company_id = aDecoder.decodeObject(forKey: "company_id") as! Int
//        self.session_id = aDecoder.decodeObject(forKey: "session_id") as! String
//        self.device_subscription_ids = aDecoder.decodeObject(forKey: "device_subscription_ids") as! [Any]
//        self.user_companies = aDecoder.decodeObject(forKey: "user_companies") as! UserCompanies
//
//    }
    
    var username: String
    var currencies: Currencies
    var uid: Int
    var fcm_project_id: String
    var db: String
    var inbox_action: Int
    var is_admin: Bool
    var partner_id: Int
    var server_version: String
    var user_context: UserContext
    var name: String
    var server_version_info: [Any]
    var company_id: Int
    var session_id: String
    var device_subscription_ids: [Any]
    var user_companies: UserCompanies
    var image: String
    var userDetails: [UserDetail]
    
    init() {
        self.username = ""
        self.currencies = Currencies()
        self.image = ""
        self.uid = 0
        self.fcm_project_id = ""
        self.db = ""
        self.inbox_action = 0
        self.is_admin = false
        self.partner_id = 0
        self.server_version = ""
        self.user_context = UserContext()
        self.name = ""
        self.server_version_info = []
        self.company_id = 0
        self.session_id = ""
        self.device_subscription_ids = []
        self.user_companies = UserCompanies()
        self.userDetails = []
    }
    
    
    init(from dict: [String:Any]) {
        self.username = dict["username"] as? String ?? ""
        self.currencies = Currencies(dict: dict["currencies"] as! [String:Any])
        self.uid = dict["uid"] as! Int
        self.fcm_project_id = dict["fcm_project_id"] as! String
        self.image = ""
        self.db = dict["db"] as! String
        self.inbox_action = dict["inbox_action"] as! Int
        self.is_admin = dict["is_admin"] as! Bool
        self.partner_id = dict["partner_id"] as! Int
        self.server_version = dict["server_version"] as! String
        self.user_context = UserContext(dict: dict["user_context"] as! [String:Any])
        self.name = dict["name"] as! String
        self.server_version_info = dict["server_version_info"] as! [Any]
        self.company_id = dict["company_id"] as! Int
        self.session_id = dict["session_id"] as! String
        self.device_subscription_ids = dict["device_subscription_ids"] as! [Any]
        self.user_companies = UserCompanies(dict: dict["user_companies"] as! [String:Any])
        self.userDetails = []
    }
    
    init(from json: JSON) {
        self.username = json["username"].string!
        self.currencies = Currencies()
        self.uid = json["uid"].int!
        self.image = ""
        self.fcm_project_id = json["fcm_project_id"].string!
        self.db = json["db"].string!
        self.inbox_action = json["inbox_action"].int!
        self.is_admin = json["is_admin"].bool!
        self.partner_id = json["partner_id"].int!
        self.server_version = json["server_version"].string!
        self.user_context = UserContext()
        self.name = json["name"].string!
        self.server_version_info = json["server_version_info"].arrayObject!
        self.company_id = json["company_id"].int!
        self.session_id = json["session_id"].string!
        self.device_subscription_ids = json["device_subscription_ids"].arrayObject!
        self.user_companies = UserCompanies()
        self.userDetails = []
    }
    
    func getImageFromAPI(with id: Int) {
        Url.params = ["params": [
            "model": "res.users",
            "method": "read",
            "args": [[id],["name","login","image_small"]],
            "kwargs": [:],
            "context": [
                "from_searchview": true,
            ]
            ]
        ]
        APIClient.instance.getAPI(with: Url.params, from: Url.Dataset, succeeded: { imageString,_ in
            if imageString != nil {
                DispatchQueue.main.async {
                    self.image = imageString!
                }
            }
        }) { error in
            print(error)
        }
    }
    
    func getUserDetailFromAPI(at offset: Int = 0, handler: (() -> ())?) {
        Url.params = ["params": [
            "model": "res.partner",
            "method": "search_read",
            "args": [[["mobile","!=",false]],["name","email","phone","mobile"],offset,20],
            "kwargs": [:],
            "context": [
                "from_searchview": true,
                ]
            ]
        ]
        APIClient.instance.getAPI(with: Url.params, from: Url.Dataset, succeeded: { _, userDetails in
            if userDetails != nil {
                DispatchQueue.main.async {
                    if userDetails!.count > 0 {
                        if handler != nil {
                            self.userDetails.append(contentsOf: userDetails!)
                            handler!()
                        }else {
                            self.userDetails.append(contentsOf: userDetails!)
                        }
                    }
                }
            }
        }) { error in
            print(error)
        }
    }
}



class UserEncoding:NSObject, NSCoding {
    var username = ""
    var name = ""
    var sessionId = ""
    var company = ""
    
    init(username: String, name: String,sessionId: String, company: String) {
        super.init()
        self.username = username
        self.name = name
        self.sessionId = sessionId
        self.company = company
        
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(username, forKey: "username")
        aCoder.encode(name, forKey: "name")
        aCoder.encode(sessionId, forKey: "sessionId")
        aCoder.encode(company, forKey: "company")
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.username = aDecoder.decodeObject(forKey: "username") as! String
        self.name = aDecoder.decodeObject(forKey: "name") as! String
        self.sessionId = aDecoder.decodeObject(forKey: "sessionId") as! String
        self.company = aDecoder.decodeObject(forKey: "company") as! String
    }
}

struct Currencies {
    var value: (x: Value,y: Value,z: Value)
    init(dict: [String:Any]) {
        value.x = Value(dict: dict["1"] as! [String:Any])
        value.y = Value(dict: dict["3"] as! [String:Any])
        value.z = Value(dict: dict["24"] as! [String:Any])
    }
    
    init() {
        value.x = Value()
        value.y = Value()
        value.z = Value()
    }
}



struct Value {
    let digits: [Int]
    let position: String
    let symbol: String
    init(dict: [String:Any]) {
        digits = dict["digits"] as! [Int]
        position = dict["position"] as! String
        symbol = dict["symbol"] as! String
    }
    init() {
        digits = []
        position = ""
        symbol = ""
    }
}

struct UserContext {
    let lang: String
    let tz: String
    let uid: Int
    
    init(dict: [String:Any]) {
        lang = dict["lang"] as! String
        tz = dict["tz"] as! String
        uid = dict["uid"] as! Int
    }
    init() {
        lang = ""
        tz = ""
        uid = 0
    }
}

struct UserCompanies {
    let current_company: [Any]
    let allowed_companies: [[Any]]
    
    init(dict: [String:Any]) {
        current_company = dict["current_company"] as! [Any]
        allowed_companies = dict["allowed_companies"] as! [[Any]]
        
    }
    init() {
        current_company = []
        allowed_companies = [[]]
    }
}


class UserDetail {
    var mobile: String
    var email: String?
    var phone: String?
    var id : Int
    var name: String
    init(from dict: [String:Any]) {
        mobile = dict["mobile"] as! String
        if let email = dict["email"] as? String {
            self.email = email
        }
        self.id = dict["id"] as! Int
        if let phone = dict["phone"] as? String {
            self.phone = phone
        }
        
        self.name = dict["name"] as! String
    }
}

