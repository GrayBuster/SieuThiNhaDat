//
//  APIClient.swift
//  SieuThiNhaDat
//
//  Created by gray buster on 8/7/18.
//  Copyright © 2018 gray buster. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class APIClient {
    public static let instance = APIClient()
    
    func getUser(with params: [String : Any] ,from url: String?, succeeded: @escaping ((User?) -> ()), failed: @escaping ((Error) -> ())) {
        if url != nil {
            Alamofire.request(url!, method: .post , parameters: params, encoding: JSONEncoding.default, headers: ["Content-Type":"application/json"]).responseJSON { response in
                switch(response.result) {
                case .success(_):
                    if let jsonValue = response.result.value as? [String:Any] , let result = jsonValue["result"] as? [String:Any] {
                        let user = User(from: result)
                        succeeded(user)
                    }
                    else {
                        succeeded(nil)
                    }
                case .failure(_):
                    failed(response.error!)
                }
            }
        }
    }
    
    func getAPI(with params: [String : Any] ,from url: String?, succeeded: @escaping ((String?,[UserDetail]?) -> ()), failed: @escaping ((Error) -> ())) {
        if url != nil {
            Alamofire.request(url!, method: .post , parameters: params, encoding: JSONEncoding.default, headers: ["Content-Type":"application/json"]).responseJSON { response in
                switch(response.result) {
                case .success(_):
                    if let jsonValue = response.result.value as? [String:Any] , let result = jsonValue["result"] as? [Any] {
                        if let resultDict = result.first as? [String:Any],
                            let imageString = resultDict["image_small"] as? String {
                            succeeded(imageString,nil)
                            
                        }else {
                            if let jsonValue = response.result.value as? [String:Any] , let result = jsonValue["result"] as? [Any] {
                                var userDetailList = [UserDetail]()
                                result.forEach { item in
                                    if let dict = item as? [String:Any] {
                                        userDetailList.append(UserDetail(from: dict))
                                    }
                                }
                                if userDetailList.count > 0 {
                                    succeeded(nil,userDetailList)
                                } else {
                                    succeeded(nil,nil)
                                }
                            }
                        }
                        
                    }
                    else {
                        print("nil")
                        succeeded(nil,nil)
                    }
                case .failure(_):
                    failed(response.error!)
                }
            }
        }
    }
    
    func checkUserIsAlreadyLogin(with params: [String : Any] ,from url: String?, succeeded: @escaping ((User?) -> ()), failed: @escaping ((Error) -> ())) {
        if url != nil {
            Alamofire.request(url!, method: .post , parameters: params, encoding: JSONEncoding.default, headers: ["Content-Type":"application/json"]).responseJSON { response in
                switch(response.result) {
                case .success(_):
                    if let jsonValue = response.result.value as? [String:Any] , let result = jsonValue["result"] as? [String:Any] {
                        let user = User(from: result)
                        succeeded(user)
                    } else {
                        succeeded(nil)
                    }
                case .failure(_):
                    failed(response.error!)
                }
            }
        }
    }
    
    
    
}
