//
//  Url.swift
//  SieuThiNhaDat
//
//  Created by gray buster on 8/10/18.
//  Copyright © 2018 gray buster. All rights reserved.
//

import Foundation

struct Url {
    static var params: [String: Any] = [:]
    static let GetUser = "https://test01.e300.vn/web/session/authenticate"
    static let Logout = "https://test01.e300.vn/web/session/destroy"
    static let SessionInfo = "https://test01.e300.vn/web/session/get_session_info"
    static let Dataset = "https://test01.e300.vn/web/dataset/call_kw"
}
