//
//  AccountUtil.swift
//  SieuThiNhaDat
//
//  Created by gray buster on 8/8/18.
//  Copyright © 2018 gray buster. All rights reserved.
//

import Foundation
import SwiftyJSON
import JsonSerializerSwift

class AccountUtil {
    
    static let instance = AccountUtil()
    
    private let userDefault = UserDefaults.standard
    
    var user: User?
    
    func saveUser(user: User) {
        let userEncode = UserEncoding(username: user.username, name: user.name, sessionId: user.session_id, company: user.user_companies.current_company[0] as! String)
        let archivedObject = archivePeople(users: [userEncode])
        userDefault.set(archivedObject, forKey: "user")
        userDefault.synchronize()
    }
    
    func getUser() -> [User]? {
        if let unarchivedObject = userDefault.object(forKey: "user") as? Data {
            return NSKeyedUnarchiver.unarchiveObject(with: unarchivedObject) as? [User]
        }
        return nil
    }
    
    func getLoginUser() {
        APIClient.instance.checkUserIsAlreadyLogin(with: Url.params, from: Url.SessionInfo, succeeded: { user in
            DispatchQueue.main.async {
                if user != nil {
                    self.user = user
                }else
                {
                    print("nil")
                }
            }
        }) { error in
            print(error)
        }
    }
    
    func clearCached() {
        userDefault.removeObject(forKey: "id")
    }
    
    func isLogin() -> Bool {
        if let _ = getUser()?.first {
            return true
        }
        return false
    }
    func archivePeople(users:[UserEncoding]) -> Data {
        let archivedObject = NSKeyedArchiver.archivedData(withRootObject: users as NSArray)
        return archivedObject
    }
    
    func convertBase64ToImage(imageString: String) -> UIImage? {
        let dataDecoded : Data = Data(base64Encoded: imageString, options: .ignoreUnknownCharacters)!
        return UIImage(data: dataDecoded)
    }
    
}
