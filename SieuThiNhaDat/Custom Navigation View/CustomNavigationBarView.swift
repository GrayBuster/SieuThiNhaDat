//
//  CustomNavigationBarView.swift
//  SieuThiNhaDat
//
//  Created by gray buster on 8/4/18.
//  Copyright © 2018 gray buster. All rights reserved.
//

import UIKit


class CustomNavigationBarView: UIView {

    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var searchButton: UIButton!
    
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("CustomNavigationView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight,.flexibleWidth]
    }
    
}
