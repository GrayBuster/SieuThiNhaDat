//
//  UserDetailTableViewCell.swift
//  SieuThiNhaDat
//
//  Created by gray buster on 8/13/18.
//  Copyright © 2018 gray buster. All rights reserved.
//

import UIKit

class UserDetailTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var mobileLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    
    var userDetail: UserDetail? {
        didSet {
            updateUI()
        }
    }
    
    private func updateUI() {
        if let user = userDetail {
            nameLabel.text = user.name
            phoneLabel.text = user.phone != nil ? user.phone! : "Không có phone"
            mobileLabel.text = user.mobile
            idLabel.text = "Id: \(user.id)"
            emailLabel.text = user.email != nil ? user.email! : "Không có email"
        }
    }
}
