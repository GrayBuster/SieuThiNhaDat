 //
//  LoginViewController.swift
//  SieuThiNhaDat
//
//  Created by gray buster on 8/7/18.
//  Copyright © 2018 gray buster. All rights reserved.
//

import UIKit

@objc protocol Login {
    @objc optional var isLogin: Bool { get set }
    @objc optional func login(email: String, password: String)
    @objc optional func logout()
}

class LoginViewController: UIViewController,Login {
    
    func login(email: String, password: String) {
        Url.params = [
            "params": [
                "db":"hhd_e300_vn_test01",
                "login":email,
                "password": password
            ]
        ]
        APIClient.instance.getUser(with: Url.params, from: Url.GetUser, succeeded: { user in
            if user != nil {
                self.user = user
                self.user?.getImageFromAPI(with: user!.uid)
                self.user?.getUserDetailFromAPI(at: 0, handler: nil)
            }
            DispatchQueue.main.async {
                if user != nil {
                    self.spinner.stopAnimating()
                    self.performSegue(withIdentifier: "Login Successful", sender: nil)
                }else {
                    self.spinner.stopAnimating()
                    self.showAlert(msg: "Bạn đã nhập sai email hoặc mật khẩu!")
                }
            }
        }) { error in
            print(error)
            DispatchQueue.main.async {
                self.spinner.stopAnimating()
                self.showAlert(msg: "Bạn không có internet hãy kiểm tra đường truyền!")
            }
            
        }
    }
    
    @IBAction func unwindToLogin(_ segue: UIStoryboardSegue) {
        
        user = nil
    }
    
    
    @IBOutlet weak var loginBtn: UIButton! {
        didSet {
            loginBtn.layer.borderColor = UIColor.purple.cgColor
            loginBtn.layer.borderWidth = 4
            loginBtn.layer.cornerRadius = 25
        }
    }
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    var tap: UITapGestureRecognizer?
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField! 
    
    var user: User?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        handleKeyboardShowHide()
    }
    
    
    
    private func handleKeyboardShowHide() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
        tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap?.cancelsTouchesInView = true
        self.view.addGestureRecognizer(tap!)
        tap?.isEnabled = false
    }
    
    @objc func keyboardWillShow(notification:NSNotification) {
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        var contentInset:UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height
        scrollView.contentInset = contentInset
        tap?.isEnabled = true
    }
    
    @objc func keyboardWillHide(notification:NSNotification) {
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInset
        tap?.isEnabled = false
    }
    
    
    @IBAction func login(_ sender: UIButton) {
        guard let email = emailTextField.text , let password = passwordTextField.text , !email.isEmpty && !password.isEmpty else { return }
        spinner.startAnimating()
        login(email: email, password: password)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Login Successful" ,
            let userInfoVC = segue.destination as? UserDetailTableViewController {
            userInfoVC.user = self.user
        }
    }

}
