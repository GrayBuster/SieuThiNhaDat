//
//  UserInfoViewController.swift
//  SieuThiNhaDat
//
//  Created by gray buster on 8/11/18.
//  Copyright © 2018 gray buster. All rights reserved.
//

import UIKit

class UserInfoViewController: UIViewController,Login {


    var user: User?
    
    @IBOutlet weak var headerView: UIView! {
        didSet {
            headerView.addBottomBorderWithColor(color: .lightGray, width: 2.0)
        }
    }
    
    
    
    @IBAction func logoutBtnClicked(_ sender: UIButton) {
        let alert = UIAlertController(title: "Cảnh báo!", message: "Bạn có muốn đăng xuất không?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Có", style: .destructive, handler: { alert in
            self.logout()
        }))
        alert.addAction(UIAlertAction(title: "Huỷ", style: .cancel, handler: { alert in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func logout() {
        Url.params = [String: Any]()
        APIClient.instance.getUser(with: Url.params, from: Url.Logout, succeeded: { user in
            if user == nil {
                DispatchQueue.main.async {
                    self.performSegue(withIdentifier: "Logout", sender: nil)
                    //self.navigationController?.dismiss(animated: true, completion: nil)
                }
            }
        }) { error in
            print(error)
            DispatchQueue.main.async {
                self.showAlert(msg: "Bạn không có internet hãy kiểm tra đường truyền!")
            }
            
        }
    }

   
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "UserInfoTableVC" , let vc = segue.destination as? UserInfoTableViewController {
            vc.user = self.user
        }
    }
   

}
