//
//  StartingViewController.swift
//  SieuThiNhaDat
//
//  Created by gray buster on 8/8/18.
//  Copyright © 2018 gray buster. All rights reserved.
//

import UIKit

class StartingViewController: UIViewController {


    var user: User?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.spinner.startAnimating()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        APIClient.instance.checkUserIsAlreadyLogin(with: Url.params, from: Url.SessionInfo, succeeded: { user in
            DispatchQueue.main.async {
                if user != nil , user!.uid > 0 {
                    self.user = user
                    self.spinner.stopAnimating()
                    self.user?.getImageFromAPI(with: user!.uid)
                    self.user?.getUserDetailFromAPI(at: 0, handler: nil)
                    self.performSegue(withIdentifier: "Already Login", sender: nil)
                } else {
                    self.spinner.stopAnimating()
                    self.performSegue(withIdentifier: "Login", sender: nil)
                }
            }
        }) { error in
            print(error)
            DispatchQueue.main.async {
                self.spinner.stopAnimating()
                let alert = UIAlertController(title: "Cảnh báo!", message: "Bạn không có kết nối internet vui lòng thử lại!", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { _ in
                    self.performSegue(withIdentifier: "Login", sender: nil)
                }))
                self.present(alert, animated: true, completion: nil)
                
            }
        }
    }
    
    @IBAction func unwind(_ segue: UIStoryboardSegue) {
        user = nil
        spinner.stopAnimating()
    }
    
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        switch segue.identifier {
        case "Already Login":
            if let userInfoVC = segue.destination as? UserDetailTableViewController,
                let user = self.user {
                userInfoVC.user = user
            }
        default:
            break
        }
    }

}
