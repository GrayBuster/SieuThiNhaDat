//
//  UserInfoTableViewController.swift
//  SieuThiNhaDat
//
//  Created by gray buster on 8/7/18.
//  Copyright © 2018 gray buster. All rights reserved.
//

import UIKit

class UserInfoTableViewController: UITableViewController,Login {
    
    @IBOutlet weak var companyLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    
    @IBOutlet weak var userDetailTableView: UITableView!
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    
    @IBOutlet var allowCompanyLabels: [UILabel]!

    
    var user: User?
    
    var userDetailDataSource = UserDetailDataSource()

    override func viewDidLoad() {
        super.viewDidLoad()
        userDetailTableView.delegate = userDetailDataSource
        userDetailTableView.dataSource = userDetailDataSource
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        spinner.startAnimating()
        if let user = user {
            companyLabel?.text = "\(user.user_companies.current_company[1])"
            nameLabel?.text = user.name
            usernameLabel?.text = user.username
            allowCompanyLabels[0].text = "\(user.user_companies.allowed_companies[0][1])"
            allowCompanyLabels[1].text = "\(user.user_companies.allowed_companies[1][1])"
            
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let user = self.user {
            if !user.image.isEmpty, user.userDetails.count > 0 {
                userDetailDataSource.userDetailList = user.userDetails
                userDetailTableView.reloadData()
                let dataDecoded : Data = Data(base64Encoded: user.image, options: .ignoreUnknownCharacters)!
                DispatchQueue.global().async {
                    if let image =  UIImage(data: dataDecoded) {
                        DispatchQueue.main.async {
                            self.imageView.image = image
                            self.spinner.stopAnimating()
                        }
                    }else {
                        DispatchQueue.main.async {
                            self.spinner.stopAnimating()
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func onBtnClicked(_ sender: UIBarButtonItem) {
        let alert = UIAlertController(title: "Cảnh báo!", message: "Bạn có muốn đăng xuất không?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Có", style: .destructive, handler: { alert in
            self.logout()
        }))
        alert.addAction(UIAlertAction(title: "Huỷ", style: .cancel, handler: { alert in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func logout() {
        APIClient.instance.getUser(with: Url.params, from: Url.Logout, succeeded: { user in
            if user == nil {
                DispatchQueue.main.async {
                    self.performSegue(withIdentifier: "Logout", sender: nil)
                }
            }
        }) { error in
            print(error)
            DispatchQueue.main.async {
                self.showAlert(msg: "Bạn không có internet hãy kiểm tra đường truyền!")
            }
            
        }
    }
}

class UserDetailDataSource: NSObject, UITableViewDataSource, UITableViewDelegate {
    
    var userDetailList = [UserDetail]()
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "User Detail Cell", for: indexPath) as! UserDetailTableViewCell
        cell.userDetail = userDetailList[indexPath.row]
        return cell
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userDetailList.count
    }
}
