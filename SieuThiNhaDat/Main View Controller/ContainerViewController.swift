//
//  ContainerViewController.swift
//  SieuThiNhaDat
//
//  Created by gray buster on 8/5/18.
//  Copyright © 2018 gray buster. All rights reserved.
//

import UIKit

class ContainerViewController: UIViewController {

    var isSideMenuVisible = false
    
    @IBOutlet weak var sideMenuConstraint: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(toggleSideMenu), name: NSNotification.Name("ToggleSideMenu"), object: nil)
    }

    @objc func toggleSideMenu() {
        if isSideMenuVisible {
            self.sideMenuConstraint.constant = 200
        } else {
            self.sideMenuConstraint.constant = 0
        }
        isSideMenuVisible = !isSideMenuVisible
        UIView.animate(withDuration: 0.3, animations: {
            
            self.view.layoutIfNeeded()
        })
    }

}
