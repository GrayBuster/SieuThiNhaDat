//
//  ViewController.swift
//  SieuThiNhaDat
//
//  Created by gray buster on 8/2/18.
//  Copyright © 2018 gray buster. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    @IBOutlet weak var mainCollectionView: UICollectionView!
    
    @IBOutlet weak var topTabBar: UITabBar! {
        didSet {
            tabBarController?.delegate = self
            topTabBar.selectedItem = topTabBar.items![0]
            topTabBar.change(fontSize: 15, font: "Arial",underlineColor: UIColor(red: 114/255, green: 188/255, blue: 93/255, alpha: 1), fontColor: .black, fontSelectedColor: .black)
            topTabBar.setupTabBarSeparators()
            tabBarController?.tabBar.change(fontSize: 15, font: "Arial",underlineColor: nil, fontColor: .lightGray, fontSelectedColor: .white)
            let numberOfItems = CGFloat((tabBarController?.tabBar.items!.count)!)
            let tabBarItemSize = CGSize(width: (tabBarController?.tabBar.frame.width)! / numberOfItems, height: (tabBarController?.tabBar.frame.height)!)
            tabBarController?.tabBar.selectionIndicatorImage = UIImage.imageWithColor(color: UIColor(red: 114/255, green: 188/255, blue: 93/255, alpha: 1), size: tabBarItemSize).resizableImage(withCapInsets: UIEdgeInsets.zero)
        }
    }
    
    @IBOutlet weak var customNavigationBarView: UIView!
    
    
    @IBOutlet weak var searchBar: UISearchBar! {
        didSet {
            self.searchBar.tintColor = UIColor.black
            searchBar.setValue("Huỷ", forKey:"_cancelButtonText")
        }
    }
    
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var tabBarView: UIView!
    
    var tabBarIndex = 0
    
    //var isVisible = false
    
    @IBOutlet weak var segmentedControl: UISegmentedControl! {
        didSet {
            segmentedControl.removeBorder()
            segmentedControl.changeBorderColor(UIColor(red: 247/255, green: 247/255, blue: 247/255, alpha: 1))
        }
    }
    
    @IBAction func selectedPage(_ sender: UISegmentedControl) {
        mainCollectionView.reloadData()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        let params = [
//            "params": [
//                "db":"hhd_e300_vn_test01",
//                "login":"hhdtest1@hhdgroup.com",
//                "password": "123123123"
//            ]
//        ]
//        APIClient.instance.getAPI(with: params, from: Url.Users, succeeded: { responseData in
//            
//            DispatchQueue.main.async {
//                if responseData.count > 0 , let result = responseData["result"] as? [String:Any] {
//                    let user = User(dict: result)
//                    //print(user.user_companies.current_company.count)
//                    print(user.name)
//                }
//            }
//        })
    }

    
    @IBAction func enableSearchBar(_ sender: UIButton) {
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            if self.searchBar.isHidden {
                self.searchBar.isHidden = false
            } else {
                self.searchBar.isHidden = true
            }
        })
    }
    @IBAction func toggleSideMenu(_ sender: UIButton) {
        NotificationCenter.default.post(name: NSNotification.Name("ToggleSideMenu"), object: nil)
//        if isVisible {
//            navigationView.backgroundColor = UIColor(red: 114/255, green: 188/255, blue: 93/255, alpha: 1)
//            tabBarView.backgroundColor = UIColor.clear
//            mainCollectionView.backgroundColor = UIColor.clear
//        } else {
//            navigationView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
//            tabBarView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
//            mainCollectionView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
//        }
//        isVisible = !isVisible
    }
    

    struct Constant {
        static var CellString = "Du An Cell"
        
    }

}


// MARK: Search Bar Delegate

extension MainViewController: UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        if searchBar.text != "" {
            searchBar.text = ""
        }
        UIView.transition(with: view, duration: 0.3, options: .transitionCrossDissolve, animations: {
            if searchBar.isHidden {
                self.searchBar.isHidden = false
            } else {
                self.searchBar.isHidden = true
            }
        })
        view.endEditing(true)
//        if searchBar.isHidden {
//            searchBar.isHidden = false
//        } else {
//            searchBar.isHidden = true
//        }
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if searchBar.text != "" {
            searchBar.text = ""
        }
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
    }
}

// MARK: Tab Bar Delegate

extension MainViewController: UITabBarDelegate,UITabBarControllerDelegate {
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        if let textTitleBarItem = item.title {
            switch textTitleBarItem {
            case "DỰ ÁN":
                Constant.CellString = "Du An Cell"
                tabBarIndex = 0
                segmentedControl.isHidden = false
            case "TIN ĐĂNG":
                Constant.CellString = "Tin Dang Cell"
                tabBarIndex = 1
                segmentedControl.isHidden = true
            default:
                break
            }
        }
        mainCollectionView.reloadData()
    }
}

// MARK: Collection View Delegate

extension MainViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    // MARK: CollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if tabBarIndex == 0 {
            return 4
        }
        return 0
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constant.CellString, for: indexPath) as! MainCollectionViewCell
        
        if tabBarIndex == 0 {
            cell.imageView.image = #imageLiteral(resourceName: "house")
            cell.locationImageView.image = #imageLiteral(resourceName: "location icon")
            cell.titleLabel.text = "Khu biệt thự cao cấp Cocoland"
            cell.decriptionLabel.text = "Đường NE4, Phường Thời Hoà, Thị xã Bến Cát, Tỉnh Bình Dương"
        } else {
            
        }

        return cell
    }
    
    // MARK: CollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if tabBarIndex == 0 ,let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "Header Cell", for: indexPath) as? SectionHeaderForMain{
            sectionHeader.headerLabel.isHidden = false
            switch segmentedControl.selectedSegmentIndex {
            case 0:
                sectionHeader.headerLabel.text = "Nhà ở xã hội"
                sectionHeader.headerLabel.textColor = UIColor(red: 136/255, green: 198/255, blue: 125/255, alpha: 1.0)
            case 1:
                sectionHeader.headerLabel.text = "Nhà ở thương mại"
                sectionHeader.headerLabel.textColor = UIColor(red: 109/255, green: 104/255, blue: 179/255, alpha: 1.0)
            case 2:
                sectionHeader.headerLabel.text = "Bất động sản cao cấp"
                sectionHeader.headerLabel.textColor = UIColor(red: 163/255, green: 97/255, blue: 112/255, alpha: 1.0)
            default:
                break
            }
            return sectionHeader
        } else {
            if let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "Header Cell", for: indexPath) as? SectionHeaderForMain {
                sectionHeader.headerLabel.isHidden = true
                
                return sectionHeader
            }
            
        }
        return UICollectionReusableView()
    }
}



