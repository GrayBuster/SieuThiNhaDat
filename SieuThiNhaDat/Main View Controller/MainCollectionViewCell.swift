//
//  TrangChuCollectionViewCell.swift
//  SieuThiNhaDat
//
//  Created by gray buster on 8/3/18.
//  Copyright © 2018 gray buster. All rights reserved.
//

import UIKit

class MainCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var locationImageView: UIImageView!
    @IBOutlet weak var decriptionLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
}
